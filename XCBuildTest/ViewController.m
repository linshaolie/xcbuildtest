//
//  ViewController.m
//  XCBuildTest
//
//  Created by shaolie on 2017/8/23.
//  Copyright © 2017年 shaolie. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    dispatch_source_t _timer;
    NSTimer *_t;
}


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSTimeInterval period = 5.0; //设置时间间隔
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_time(DISPATCH_TIME_NOW, 5.0 * NSEC_PER_SEC), period * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(_timer, ^{
        //在这里执行事件
        NSLog(@"111");
    });
    dispatch_resume(_timer);
    
    
    dispatch_block_t task = dispatch_block_create(DISPATCH_BLOCK_BARRIER, ^{
        NSLog(@"GCD can cancel block");
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), task);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        dispatch_block_cancel(task);
    });
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        dispatch_suspend(_timer);
    });
    
    _t = [NSTimer timerWithTimeInterval:5 repeats:NO block:^(NSTimer * _Nonnull timer) {
        NSLog(@"222");
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_t invalidate];
    });
    
#if DEBUG
        [self presentViewController:[UIAlertController alertControllerWithTitle:@"Debug" message:@"this is debug env" preferredStyle:(UIAlertControllerStyleAlert)] animated:YES completion:nil];
#endif
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
